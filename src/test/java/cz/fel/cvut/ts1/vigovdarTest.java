package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class vigovdarTest {

    @Test
    void factorial() {
        final vigovdar test =new vigovdar();
        long one =test.factorial(1);
        long two =test.factorial(2);
        long three =test.factorial(3);

        assertEquals(one,1);
        assertEquals(two,2);
        assertEquals(three,6);

    }
}