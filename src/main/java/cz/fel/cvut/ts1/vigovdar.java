package cz.fel.cvut.ts1;

public class vigovdar {
    public long factorial(int n){
        long fact = 1;
        for (int i=1;i<=n;i++){
            fact=fact*i;
        }
        return fact;
    }

    public long factorial2(int n) {
        if (n == 0)
                return 1;
        else return n * factorial2(n-1);
    }
}
